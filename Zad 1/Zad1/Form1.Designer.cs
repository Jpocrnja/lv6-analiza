﻿namespace Zad1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zbrajanje = new System.Windows.Forms.Button();
            this.oduzimanje = new System.Windows.Forms.Button();
            this.mnozenje = new System.Windows.Forms.Button();
            this.djeljenje = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.cosinus = new System.Windows.Forms.Button();
            this.logaritam = new System.Windows.Forms.Button();
            this.potenciranje = new System.Windows.Forms.Button();
            this.korjenovanje = new System.Windows.Forms.Button();
            this.txtB_x = new System.Windows.Forms.TextBox();
            this.txtB_y = new System.Windows.Forms.TextBox();
            this.rezultat = new System.Windows.Forms.Label();
            this.quit = new System.Windows.Forms.Button();
            this.AC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // zbrajanje
            // 
            this.zbrajanje.Location = new System.Drawing.Point(60, 173);
            this.zbrajanje.Name = "zbrajanje";
            this.zbrajanje.Size = new System.Drawing.Size(75, 23);
            this.zbrajanje.TabIndex = 0;
            this.zbrajanje.Text = "+";
            this.zbrajanje.UseVisualStyleBackColor = true;
            this.zbrajanje.Click += new System.EventHandler(this.zbrajanje_Click);
            // 
            // oduzimanje
            // 
            this.oduzimanje.Location = new System.Drawing.Point(160, 173);
            this.oduzimanje.Name = "oduzimanje";
            this.oduzimanje.Size = new System.Drawing.Size(75, 23);
            this.oduzimanje.TabIndex = 1;
            this.oduzimanje.Text = "-";
            this.oduzimanje.UseVisualStyleBackColor = true;
            this.oduzimanje.Click += new System.EventHandler(this.oduzimanje_Click);
            // 
            // mnozenje
            // 
            this.mnozenje.Location = new System.Drawing.Point(60, 225);
            this.mnozenje.Name = "mnozenje";
            this.mnozenje.Size = new System.Drawing.Size(75, 23);
            this.mnozenje.TabIndex = 2;
            this.mnozenje.Text = "*";
            this.mnozenje.UseVisualStyleBackColor = true;
            this.mnozenje.Click += new System.EventHandler(this.mnozenje_Click);
            // 
            // djeljenje
            // 
            this.djeljenje.Location = new System.Drawing.Point(160, 225);
            this.djeljenje.Name = "djeljenje";
            this.djeljenje.Size = new System.Drawing.Size(75, 23);
            this.djeljenje.TabIndex = 3;
            this.djeljenje.Text = "/";
            this.djeljenje.UseVisualStyleBackColor = true;
            this.djeljenje.Click += new System.EventHandler(this.djeljenje_Click);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(60, 278);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(75, 23);
            this.sinus.TabIndex = 4;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // cosinus
            // 
            this.cosinus.Location = new System.Drawing.Point(160, 278);
            this.cosinus.Name = "cosinus";
            this.cosinus.Size = new System.Drawing.Size(75, 23);
            this.cosinus.TabIndex = 5;
            this.cosinus.Text = "cos";
            this.cosinus.UseVisualStyleBackColor = true;
            this.cosinus.Click += new System.EventHandler(this.cosinus_Click);
            // 
            // logaritam
            // 
            this.logaritam.Location = new System.Drawing.Point(277, 278);
            this.logaritam.Name = "logaritam";
            this.logaritam.Size = new System.Drawing.Size(75, 23);
            this.logaritam.TabIndex = 6;
            this.logaritam.Text = "log";
            this.logaritam.UseVisualStyleBackColor = true;
            this.logaritam.Click += new System.EventHandler(this.logaritam_Click);
            // 
            // potenciranje
            // 
            this.potenciranje.Location = new System.Drawing.Point(60, 326);
            this.potenciranje.Name = "potenciranje";
            this.potenciranje.Size = new System.Drawing.Size(75, 23);
            this.potenciranje.TabIndex = 7;
            this.potenciranje.Text = "pow";
            this.potenciranje.UseVisualStyleBackColor = true;
            this.potenciranje.Click += new System.EventHandler(this.potenciranje_Click);
            // 
            // korjenovanje
            // 
            this.korjenovanje.Location = new System.Drawing.Point(160, 326);
            this.korjenovanje.Name = "korjenovanje";
            this.korjenovanje.Size = new System.Drawing.Size(75, 23);
            this.korjenovanje.TabIndex = 8;
            this.korjenovanje.Text = "sqrt";
            this.korjenovanje.UseVisualStyleBackColor = true;
            this.korjenovanje.Click += new System.EventHandler(this.korjenovanje_Click);
            // 
            // txtB_x
            // 
            this.txtB_x.Location = new System.Drawing.Point(60, 45);
            this.txtB_x.Name = "txtB_x";
            this.txtB_x.Size = new System.Drawing.Size(100, 20);
            this.txtB_x.TabIndex = 9;
            // 
            // txtB_y
            // 
            this.txtB_y.Location = new System.Drawing.Point(60, 98);
            this.txtB_y.Name = "txtB_y";
            this.txtB_y.Size = new System.Drawing.Size(100, 20);
            this.txtB_y.TabIndex = 10;
            // 
            // rezultat
            // 
            this.rezultat.AutoSize = true;
            this.rezultat.Location = new System.Drawing.Point(317, 98);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(41, 13);
            this.rezultat.TabIndex = 11;
            this.rezultat.Text = "rezultat";
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(277, 326);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(75, 23);
            this.quit.TabIndex = 12;
            this.quit.Text = "quit";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // AC
            // 
            this.AC.Location = new System.Drawing.Point(277, 225);
            this.AC.Name = "AC";
            this.AC.Size = new System.Drawing.Size(75, 23);
            this.AC.TabIndex = 13;
            this.AC.Text = "AC";
            this.AC.UseVisualStyleBackColor = true;
            this.AC.Click += new System.EventHandler(this.AC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.AC);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.txtB_y);
            this.Controls.Add(this.txtB_x);
            this.Controls.Add(this.korjenovanje);
            this.Controls.Add(this.potenciranje);
            this.Controls.Add(this.logaritam);
            this.Controls.Add(this.cosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.djeljenje);
            this.Controls.Add(this.mnozenje);
            this.Controls.Add(this.oduzimanje);
            this.Controls.Add(this.zbrajanje);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zbrajanje;
        private System.Windows.Forms.Button oduzimanje;
        private System.Windows.Forms.Button mnozenje;
        private System.Windows.Forms.Button djeljenje;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button cosinus;
        private System.Windows.Forms.Button logaritam;
        private System.Windows.Forms.Button potenciranje;
        private System.Windows.Forms.Button korjenovanje;
        private System.Windows.Forms.TextBox txtB_x;
        private System.Windows.Forms.TextBox txtB_y;
        private System.Windows.Forms.Label rezultat;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Button AC;
    }
}

