﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void zbrajanje_Click(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else
            {
                rezultat.Text = (x + y).ToString();
            }
        }

        private void oduzimanje_Click(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else
            {
                rezultat.Text = (x - y).ToString();
            }
        }

        private void mnozenje_Click(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else
            {
                rezultat.Text = (x * y).ToString();
            }
        }

        private void djeljenje_Click(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else if (y == 0)
                MessageBox.Show("Nazivnik ne smije biti 0!");
            else
            {
                rezultat.Text = (x / y).ToString();
            }
        }

        private void sinus_Click(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else
            {
                txtB_y.Clear();
                rezultat.Text = (Math.Sin(x * Math.PI / 180)).ToString();
            }
        }

        private void cosinus_Click(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else
            {
                txtB_y.Clear();
                rezultat.Text = (Math.Cos(x * Math.PI / 180)).ToString();
            }
        }

        private void logaritam_Click(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (x <= 0)
                MessageBox.Show("X mora biti veci od 0!");
            else
            {
                txtB_y.Clear();
                rezultat.Text = (Math.Log10(x)).ToString();
            }
        }

        private void potenciranje_Click(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else
            {
                txtB_y.Clear();
                rezultat.Text = (x * x).ToString();
            }
        }

        private void korjenovanje_Click(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (x < 0) MessageBox.Show("Vrijednost pod korijenom ne smije biti negativna!");
            else
            {
                txtB_y.Clear();
                rezultat.Text = (Math.Sqrt(x)).ToString();
            }
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AC_Click(object sender, EventArgs e)
        {
            txtB_x.Clear();
            txtB_y.Clear();
            rezultat.Text = "Rezultat";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
